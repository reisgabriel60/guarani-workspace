import { Module } from '@nestjs/common';
import { APP_GUARD } from '@nestjs/core';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AppController } from './app.controller';
import { AuditModule } from './audit/audit.module';
import { CommonModule } from './common/common.module';
import { AuthModule } from './modules/auth/auth.module';
import { JwtAuthGuard } from './modules/auth/jwt-auth.guard';
import { CommercialModule } from './modules/commercial/commercial.module';
import { DeveloperModule } from './modules/developer/developer.module';
import { ImagesModule } from './modules/images/images.module';
import { ManagementModule } from './modules/management/management.module';
import { SettingModule } from './modules/setting/setting.module';
import { SiteModule } from './modules/site/site.module';
import { SupportModule } from './modules/support/support.module';
import { environment } from '../environments/environment';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: environment.POSTGRES_HOST,
      port: +environment.POSTGRES_PORT,
      username: environment.POSTGRES_USER,
      password: environment.POSTGRES_PASSWORD,
      database: environment.POSTGRES_DATABASE,
      autoLoadEntities: true,
      synchronize: true,
      subscribers: ['./audit/audit.subscriber.ts'],
      logging: !environment.production,
    }),
    CommonModule,
    SiteModule,
    AuthModule,
    DeveloperModule,
    SupportModule,
    CommercialModule,
    ManagementModule,
    SettingModule,
    AuditModule,
    ImagesModule,
  ],
  controllers: [AppController],
  providers: [
    {
      provide: APP_GUARD,
      useClass: JwtAuthGuard,
    },
  ],
})
export class AppModule {}
