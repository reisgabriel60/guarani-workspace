import { AuditInterface } from '@workspace/models';
import { IsOptional, IsString } from 'class-validator';

export class AuditDto implements AuditInterface {
  @IsOptional()
  id?: string;

  @IsString()
  modelName: string;

  @IsString()
  methodName: string;

  @IsOptional()
  oldObject?: string;

  @IsOptional()
  newObject?: string;

  @IsOptional()
  createdAt?: Date;
}
