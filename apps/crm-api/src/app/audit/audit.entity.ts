import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export abstract class Audit {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ type: 'timestamp', default: () => 'CURRENT_TIMESTAMP' })
  createdAt: Date;

  @Column()
  methodName: string;

  @Column()
  modelName: string;

  @Column({ nullable: true, type: 'text' })
  oldObject?: string;

  @Column({ nullable: true, type: 'text' })
  newObject?: string;
}
