import {
  DataSource,
  InsertEvent,
  RecoverEvent,
  SoftRemoveEvent,
} from 'typeorm';
import { EventSubscriber } from 'typeorm/decorator/listeners/EventSubscriber';
import { EntitySubscriberInterface } from 'typeorm/subscriber/EntitySubscriberInterface';
import { RemoveEvent } from 'typeorm/subscriber/event/RemoveEvent';
import { UpdateEvent } from 'typeorm/subscriber/event/UpdateEvent';

import { AuditDto } from './audit.dto';
import { Audit } from './audit.entity';
import { AuditService } from './audit.service';

@EventSubscriber()
export class AuditSubscriber implements EntitySubscriberInterface {
  constructor(dataSource: DataSource, private auditService: AuditService) {
    dataSource.subscribers.push(this);
  }

  private log(event, methodName: string) {
    if (event.metadata.name === Audit.name) return;
    const audit: AuditDto = new AuditDto();
    audit.methodName = methodName;
    audit.modelName = event.metadata.name;
    if (event.entity) audit.newObject = JSON.stringify(event.entity);
    if (event.databaseEntity)
      audit.oldObject = JSON.stringify(event.databaseEntity);
    this.auditService.create(audit);
  }

  beforeInsert(event: InsertEvent<any>) {
    this.log(event, 'insert');
  }
  beforeUpdate(event: UpdateEvent<any>) {
    this.log(event, 'update');
  }
  beforeRemove(event: RemoveEvent<any>) {
    this.log(event, 'remove');
  }
  beforeSoftRemove(event: SoftRemoveEvent<any>) {
    this.log(event, 'softRemove');
  }
  beforeRecover(event: RecoverEvent<any>) {
    this.log(event, 'recover');
  }
}
