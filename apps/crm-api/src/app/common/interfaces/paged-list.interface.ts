export interface PagedList<T> {
  totalCount: number;
  data: T[];
}
