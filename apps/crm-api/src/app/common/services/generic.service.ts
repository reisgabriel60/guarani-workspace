import { Injectable } from '@nestjs/common';
import { FilterInteface, PaginatedResponseInterface } from '@workspace/models';
import {
  Equal,
  FindOperator,
  FindOptionsWhere,
  LessThanOrEqual,
  ILike,
  MoreThanOrEqual,
  Not,
} from 'typeorm';

@Injectable()
export abstract class GenericService<T> {
  buildWhere(filters: FilterInteface[]) {
    function setWhere(where, columns: string[], value: FindOperator<string>) {
      if (columns.length === 1) {
        where[columns[0]] = value;
      } else {
        const column = columns.shift();
        where[column] = {};
        setWhere(where[column], columns, value);
      }
    }

    if (filters) {
      const where: FindOptionsWhere<T> = {};
      for (const filter of filters) {
        switch (filter.filterType) {
          case '==':
            setWhere(where, filter.column.split('.'), Equal(filter.value));
            break;
          case '!=':
            setWhere(where, filter.column.split('.'), Not(filter.value));
            break;
          case '<=':
            setWhere(
              where,
              filter.column.split('.'),
              LessThanOrEqual(filter.value),
            );
            break;
          case '>=':
            setWhere(
              where,
              filter.column.split('.'),
              MoreThanOrEqual(filter.value),
            );
            break;
          case 'contem':
            setWhere(
              where,
              filter.column.split('.'),
              ILike('%' + filter.value + '%'),
            );
            break;
          default:
            setWhere(where, filter.column.split('.'), Equal(filter.value));
            break;
        }
      }
      return where;
    } else {
      return null;
    }
  }

  abstract findAll(
    filters: FilterInteface[],
    page: number,
    limit: number,
  ): Promise<PaginatedResponseInterface<T>>;
}
