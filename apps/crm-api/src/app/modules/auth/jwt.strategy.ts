import { Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { ExtractJwt, Strategy } from 'passport-jwt';

import { environment } from '../../../environments/environment';
import { UpdateUserDto } from '../user/dto/update-user.dto';

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor() {
    super({
      jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
      ignoreExpiration: false,
      secretOrKey: environment.JWT.SECRET,
    });
  }

  async validate(payload: UpdateUserDto) {
    return { userId: payload.id, username: payload.email };
  }
}
