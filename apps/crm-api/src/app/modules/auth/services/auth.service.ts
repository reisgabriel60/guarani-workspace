import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';

import { UpdateUserDto } from '../../user/dto/update-user.dto';
import { UserService } from '../../user/services/user.service';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) {}

  async validateUser(email: string, pass: string): Promise<UpdateUserDto> {
    const user = await this.userService.findOneByEmail(email);
    if (user && user.active) {
      const passwdValid = await bcrypt.compare(pass, user.password);
      if (passwdValid) {
        const { ...result } = user;
        delete result.password;
        delete result.createdAt;
        delete result.updatedAt;
        return result;
      } else {
        throw new UnauthorizedException('Usuário ou senha inválidos.');
      }
    } else if (user) {
      throw new UnauthorizedException('Usuário ou senha inválidos.');
    } else {
      throw new UnauthorizedException('Usuário ou senha inválidos.');
    }
  }

  async login(user: UpdateUserDto) {
    const payload = await this.validateUser(user.email, user.password);
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
