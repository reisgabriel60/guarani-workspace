import { Module } from '@nestjs/common';

import { BillingModule } from './billing/billing.module';
import { CustomerModule } from './customer/customer.module';

@Module({
  controllers: [],
  providers: [],
  imports: [CustomerModule, BillingModule],
})
export class CommercialModule {}
