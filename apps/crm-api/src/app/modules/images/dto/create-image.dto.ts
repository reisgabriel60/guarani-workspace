import { IsString } from 'class-validator';

export class CreateImageDto {
  @IsString()
  key: string;

  @IsString()
  url: string;

  @IsString()
  ETag: string;

  @IsString()
  imageSize: number;

  @IsString()
  bucketName: string;
}
