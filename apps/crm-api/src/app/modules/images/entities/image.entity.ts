import { ImageInterface } from '@workspace/models';
import { Column, Entity } from 'typeorm';

import { EntityBase } from '../../../common/entities/entity-base.entity';

@Entity()
export class Image extends EntityBase implements ImageInterface {
  @Column()
  key: string;

  @Column()
  url: string;

  @Column()
  ETag: string;

  @Column()
  imageSize: number;

  @Column()
  bucketName: string;
}
