import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Image } from './entities/image.entity';
import { ImagesController } from './images.controller';
import { ImagesService } from './services/images.service';
import { S3Service } from './services/s3.service';

@Module({
  imports: [TypeOrmModule.forFeature([Image])],
  controllers: [ImagesController],
  providers: [ImagesService, S3Service],
  exports: [ImagesService],
})
export class ImagesModule {}
