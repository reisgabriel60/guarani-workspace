import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FilterInteface, PaginatedResponseInterface } from '@workspace/models';
import { FindOptionsWhere, Repository } from 'typeorm';

import { GenericService } from '../../../common/services/generic.service';
import { CreateImageDto } from '../dto/create-image.dto';
import { Image } from '../entities/image.entity';
import { S3Service } from './s3.service';

@Injectable()
export class ImagesService extends GenericService<Image> {
  constructor(
    private readonly s3Service: S3Service,
    @InjectRepository(Image)
    private imageRepository: Repository<Image>,
  ) {
    super();
  }

  async create({ buffer, originalname, mimetype, size }: Express.Multer.File) {
    const upResult = await this.s3Service.upload(
      buffer,
      originalname,
      mimetype,
    );
    const newImage: CreateImageDto = {
      imageSize: size,
      ETag: upResult.ETag,
      url: upResult.Location,
      key: upResult.Key,
      bucketName: upResult.Bucket,
    };
    return this.imageRepository.save(newImage);
  }

  async findAll(filters: FilterInteface[], page: number, limit: number) {
    const where: FindOptionsWhere<Image> = super.buildWhere(filters);
    const response: PaginatedResponseInterface<Image> = {
      data: await this.imageRepository.find({
        take: limit,
        skip: (page - 1) * limit,
        where,
      }),
      totalCount: await this.imageRepository.count({ where }),
    };
    return response;
  }

  async findOne(id: string) {
    return this.imageRepository.findOneBy({ id });
  }

  async remove(id: string) {
    const image = await this.findOne(id);
    if (image) {
      try {
        await this.s3Service.delete(image.bucketName, image.key);
        return await this.imageRepository.delete(id);
      } catch (e) {
        throw new BadRequestException(e, 'Não foi possivel deletar o arquivo.');
      }
    } else {
      throw new BadRequestException(null, 'Arquivo não encontrado.');
    }
  }
}
