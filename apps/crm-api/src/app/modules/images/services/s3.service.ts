import { Injectable } from '@nestjs/common';
import { S3 } from 'aws-sdk';
import { v4 as uuid } from 'uuid';
import {environment} from "../../../../environments/environment";

@Injectable()
export class S3Service {
  AWS_S3_BUCKET = environment.AWS_S3.BUCKET;
  s3 = new S3({
    accessKeyId: environment.AWS_S3.KEY_ID,
    secretAccessKey: environment.AWS_S3.KEY_PASS,
  });

  async upload(file, name, mimetype) {
    const params = {
      Bucket: this.AWS_S3_BUCKET,
      Key: uuid() + '-' + name,
      Body: file,
      ContentType: mimetype,
      ACL: 'public-read',
      ContentDisposition: 'inline',
      CreateBucketConfiguration: {
        LocationConstraint: 'us-east-1',
      },
    };

    try {
      return await this.s3.upload(params).promise();
    } catch (e) {
      console.log(e);
    }
  }

  delete(Bucket, Key) {
    return this.s3.deleteObject({ Bucket, Key }).promise();
  }
}
