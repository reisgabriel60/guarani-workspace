import { Module } from '@nestjs/common';

import { SubscriptionModule } from './subscription/subscription.module';
import { UserModule } from './user/user.module';

@Module({
  controllers: [],
  providers: [],
  imports: [SubscriptionModule, UserModule],
})
export class ManagementModule {}
