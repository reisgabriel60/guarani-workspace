import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { PaginatedRequestInterface } from '@workspace/models';

import { AlbunsService } from './albuns.service';
import { CreateAlbumDto } from './dto/create-album.dto';
import { Public } from '../../../public.decorator';
import { multerOptions } from '../../images/images.controller';

@Controller('albuns')
export class AlbunsController {
  constructor(private readonly albunsService: AlbunsService) {}

  @Post()
  create(@Body() createCompanyDto: CreateAlbumDto) {
    return this.albunsService.create(createCompanyDto);
  }

  @Post('foto/:id')
  @UseInterceptors(FileInterceptor('image', multerOptions))
  addFoto(@UploadedFile() image: Express.Multer.File, @Param('id') id: string) {
    return this.albunsService.addFoto(id, image);
  }

  @Public()
  @Get()
  findAll(
    @Query() { filters, page = 1, limit = 10 }: PaginatedRequestInterface,
  ) {
    if (filters) {
      filters = JSON.parse(filters.toString());
    }
    return this.albunsService.findAll(filters, page, limit);
  }

  @Public()
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.albunsService.findOne(id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() createCompanyDto: CreateAlbumDto) {
    return this.albunsService.update(id, createCompanyDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.albunsService.remove(id);
  }
}
