import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { AlbunsController } from './albuns.controller';
import { AlbunsService } from './albuns.service';
import { Album } from './entities/album.entity';
import { Foto } from './entities/foto.entity';
import { ImagesModule } from '../../images/images.module';

@Module({
  imports: [TypeOrmModule.forFeature([Album, Foto]), ImagesModule],
  controllers: [AlbunsController],
  providers: [AlbunsService],
})
export class AlbunsModule {}
