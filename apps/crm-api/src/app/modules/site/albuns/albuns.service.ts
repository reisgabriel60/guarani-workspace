import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FilterInteface, PaginatedResponseInterface } from '@workspace/models';
import { FindOptionsWhere, Repository } from 'typeorm';

import { CreateAlbumDto } from './dto/create-album.dto';
import { Album } from './entities/album.entity';
import { Foto } from './entities/foto.entity';
import { GenericService } from '../../../common/services/generic.service';
import { ImagesService } from '../../images/services/images.service';

@Injectable()
export class AlbunsService extends GenericService<Album> {
  constructor(
    @InjectRepository(Album)
    private readonly albumEntityRepository: Repository<Album>,
    @InjectRepository(Foto)
    private readonly fotoEntityRepository: Repository<Foto>,
    private readonly imagesService: ImagesService,
  ) {
    super();
  }

  async create(createAlbumDto: CreateAlbumDto) {
    return this.albumEntityRepository.save(createAlbumDto);
  }

  async addFoto(id: string, image) {
    const fotoDto = this.fotoEntityRepository.create();
    fotoDto.album = await this.albumEntityRepository.findOneBy({ id });
    if (!fotoDto.album)
      throw new NotFoundException(null, 'Album nao encontrado');
    fotoDto.image = await this.imagesService.create(image);
    return this.fotoEntityRepository.save(fotoDto);
  }

  async findAll(filters: FilterInteface[], page: number, limit: number) {
    const where: FindOptionsWhere<Album> = super.buildWhere(filters);
    const response: PaginatedResponseInterface<Album> = {
      data: await this.albumEntityRepository.find({
        take: limit,
        skip: (page - 1) * limit,
        where,
      }),
      totalCount: await this.albumEntityRepository.count({ where }),
    };
    return response;
  }

  async findOne(id: string) {
    return await this.albumEntityRepository.findOne({ where: { id } });
  }

  async update(id: string, createAlbumDto: CreateAlbumDto) {
    return this.albumEntityRepository.update(id, createAlbumDto);
  }

  async remove(id: string) {
    return await this.albumEntityRepository.delete(id);
  }
}
