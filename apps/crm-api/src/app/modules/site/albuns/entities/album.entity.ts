import { AlbumInterface } from '@workspace/models';
import { Column, Entity, OneToMany, JoinTable } from 'typeorm';

import { Foto } from './foto.entity';
import { EntityBase } from '../../../../common/entities/entity-base.entity';

@Entity()
export class Album extends EntityBase implements AlbumInterface {
  @Column()
  title: string;
  @OneToMany(() => Foto, (foto) => foto.album, {
    cascade: true,
    eager: true,
  })
  @JoinTable()
  fotos: Foto[];
}
