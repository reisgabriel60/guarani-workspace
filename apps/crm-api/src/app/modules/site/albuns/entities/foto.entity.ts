import { FotoInterface } from '@workspace/models';
import { Entity, JoinColumn, ManyToOne, OneToOne } from 'typeorm';

import { Album } from './album.entity';
import { EntityBase } from '../../../../common/entities/entity-base.entity';
import { Image } from '../../../images/entities/image.entity';

@Entity()
export class Foto extends EntityBase implements FotoInterface {
  @ManyToOne(() => Album, (albumEntity) => albumEntity.fotos, {
    onDelete: 'CASCADE',
  })
  album: Album;

  @OneToOne(() => Image, {
    cascade: true,
    eager: true,
  })
  @JoinColumn()
  image: Image;
}
