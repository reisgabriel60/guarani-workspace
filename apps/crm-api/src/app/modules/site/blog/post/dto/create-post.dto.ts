import { BlogPostInterface, ImageInterface } from '@workspace/models';
import { IsBoolean, IsNotEmpty, IsString } from 'class-validator';
export class CreatePostDto implements BlogPostInterface {
  @IsNotEmpty({ message: 'O titulo não pode estar vazio.' })
  @IsString({ message: 'O titulo deve ser uma String.' })
  title: string;

  slug: string;

  @IsNotEmpty({ message: 'O conteudo não pode estar vazio.' })
  @IsString()
  content: string;

  img: string | null;

  image: ImageInterface;

  @IsBoolean()
  isMainBanner: boolean;

  createdAt: Date;

  updatedAt?: Date;
}
