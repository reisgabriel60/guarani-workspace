import { BlogPostInterface } from '@workspace/models';
import { Column, Entity, JoinColumn, OneToOne } from 'typeorm';

import { EntityBase } from '../../../../../common/entities/entity-base.entity';
import { Image } from '../../../../images/entities/image.entity';

@Entity()
export class Post extends EntityBase implements BlogPostInterface {
  @Column()
  title: string;

  @Column()
  content: string;

  @Column({ unique: true })
  slug: string;

  @OneToOne(() => Image, {
    cascade: true,
    eager: true,
  })
  @JoinColumn()
  image: Image;

  @Column()
  isMainBanner: boolean;
}
