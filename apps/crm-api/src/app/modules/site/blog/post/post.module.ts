import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ImagesModule } from '../../../images/images.module';
import { PostController } from './controllers/post.controller';
import { Post } from './entities/post.entity';
import { PostService } from './services/post.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Post]),
    ImagesModule,
  ],
  controllers: [PostController],
  providers: [PostService],
})
export class PostModule {}
