import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { FilterInteface } from '@workspace/models';
import { FindOptionsWhere, Repository } from 'typeorm';

import { GenericService } from '../../../../../common/services/generic.service';
import { ImagesService } from '../../../../images/services/images.service';
import { CreatePostDto } from '../dto/create-post.dto';
import { UpdatePostDto } from '../dto/update-post.dto';
import { Post } from '../entities/post.entity';

@Injectable()
export class PostService extends GenericService<Post> {
  constructor(
    @InjectRepository(Post)
    private readonly postRepository: Repository<Post>,
    private readonly imagesService: ImagesService,
  ) {
    super();
  }

  async create(createPostDto: CreatePostDto, image) {
    createPostDto.slug = this.generateSlug(createPostDto.title);
    const where = { slug: createPostDto.slug };
    const totalCount = await this.postRepository.count({ where });
    if (totalCount > 0) {
      throw new HttpException(
        'Já existe um Post com este título.',
        HttpStatus.BAD_REQUEST,
      );
    }
    if (image) {
      createPostDto.image = await this.imagesService.create(image);
    }

    return await this.postRepository.save(createPostDto);
  }

  async findAll(filtros: FilterInteface[], pagina: number, limit: number) {
    const where: FindOptionsWhere<Post> = super.buildWhere(filtros);
    return {
      data: await this.postRepository.find({
        take: limit,
        skip: (pagina - 1) * limit,
        where,
        order: { createdAt: 'DESC' },
      }),
      totalCount: await this.postRepository.count({ where }),
    };
  }

  async findOne(id: string) {
    return await this.postRepository.findOneBy({ id });
  }

  async update(id: string, updatePostDto: UpdatePostDto, image) {
    delete updatePostDto.createdAt;
    updatePostDto.updatedAt = new Date();

    if (updatePostDto.title) {
      updatePostDto.slug = this.generateSlug(updatePostDto.title);
    }
    if (image) {
      updatePostDto.image = await this.imagesService.create(image);
    }

    return await this.postRepository.update(id, updatePostDto);
  }

  async remove(id: string) {
    return await this.postRepository.delete(id);
  }

  private generateSlug(title: string): string {
    return title.trim().replace(/ +/g, '-').toLocaleLowerCase();
  }
}
