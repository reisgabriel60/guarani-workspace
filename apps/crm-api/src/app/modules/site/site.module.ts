import { Module } from '@nestjs/common';

import { AlbunsModule } from './albuns/albuns.module';
import { PostModule } from './blog/post/post.module';

@Module({
  imports: [PostModule, AlbunsModule],
})
export class SiteModule {}
