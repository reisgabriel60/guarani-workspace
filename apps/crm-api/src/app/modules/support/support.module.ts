import { Module } from '@nestjs/common';

import { ChatModule } from './chat/chat.module';
import { ReportModule } from './report/report.module';
import { WebserviceModule } from './webservice/webservice.module';

@Module({
  controllers: [],
  providers: [],
  imports: [WebserviceModule, ReportModule, ChatModule],
})
export class SupportModule {}
