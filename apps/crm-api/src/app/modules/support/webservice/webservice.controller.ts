import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';

import { CreateWebserviceDto } from './dto/create-webservice.dto';
import { UpdateWebserviceDto } from './dto/update-webservice.dto';
import { WebserviceService } from './webservice.service';

@Controller('webservice')
export class WebserviceController {
  constructor(private readonly webserviceService: WebserviceService) {}

  @Post()
  create(@Body() createWebserviceDto: CreateWebserviceDto) {
    return this.webserviceService.create(createWebserviceDto);
  }

  @Get()
  findAll() {
    return this.webserviceService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.webserviceService.findOne(+id);
  }

  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateWebserviceDto: UpdateWebserviceDto,
  ) {
    return this.webserviceService.update(+id, updateWebserviceDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.webserviceService.remove(+id);
  }
}
