import { Module } from '@nestjs/common';

import { WebserviceController } from './webservice.controller';
import { WebserviceService } from './webservice.service';

@Module({
  controllers: [WebserviceController],
  providers: [WebserviceService],
})
export class WebserviceModule {}
