import { Injectable } from '@nestjs/common';

import { CreateWebserviceDto } from './dto/create-webservice.dto';
import { UpdateWebserviceDto } from './dto/update-webservice.dto';

@Injectable()
export class WebserviceService {
  create(createWebserviceDto: CreateWebserviceDto) {
    return 'This action adds a new webservice';
  }

  findAll() {
    return `This action returns all webservice`;
  }

  findOne(id: number) {
    return `This action returns a #${id} webservice`;
  }

  update(id: number, updateWebserviceDto: UpdateWebserviceDto) {
    return `This action updates a #${id} webservice`;
  }

  remove(id: number) {
    return `This action removes a #${id} webservice`;
  }
}
