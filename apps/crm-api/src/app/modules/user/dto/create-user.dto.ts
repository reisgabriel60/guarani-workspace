import { Exclude } from 'class-transformer';
import { IsString } from 'class-validator';

export class CreateUserDto {
  id: string;

  @IsString()
  name: string;

  @IsString()
  email: string;

  @IsString()
  @Exclude({ toPlainOnly: true })
  password: string;

  createdAt: Date;

  updatedAt?: Date;
}
