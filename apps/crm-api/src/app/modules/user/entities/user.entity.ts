import { Exclude } from 'class-transformer';
import { Column, Entity } from 'typeorm';

import { EntityBase } from '../../../common/entities/entity-base.entity';

@Entity()
export class User extends EntityBase {
  @Column({ nullable: false })
  name: string;

  @Column({ unique: true, nullable: false })
  email: string;

  @Exclude({ toPlainOnly: true })
  @Column({ nullable: false })
  password: string;

  @Column({ default: true })
  active: boolean;
}
