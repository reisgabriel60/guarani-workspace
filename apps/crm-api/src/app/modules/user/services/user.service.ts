import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';

import { CreateUserDto } from '../dto/create-user.dto';
import { UpdateUserDto } from '../dto/update-user.dto';
import { User } from '../entities/user.entity';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User)
    private readonly userRepository: Repository<User>,
  ) {}
  async create(createUserDto: CreateUserDto) {
    createUserDto.password = await bcrypt.hash(createUserDto.password, 7);
    return await this.userRepository.save(createUserDto);
  }

  async findAll() {
    return {
      data: await this.userRepository.find({ take: 10 }),
      totalCount: await this.userRepository.count(),
    };
  }

  async findOneById(id: string) {
    if (id) {
      return await this.userRepository.findOneBy({ id });
    }
    return null;
  }

  async findOneByName(name: string) {
    if (name) {
      return await this.userRepository.findOneBy({ name });
    }
    return null;
  }

  async findOneByEmail(email: string) {
    if (email) {
      return await this.userRepository.findOneBy({ email });
    }
    return null;
  }

  async update(id: string, updateUserDto: UpdateUserDto) {
    return await this.userRepository.update(id, updateUserDto);
  }

  async remove(id: string) {
    return await this.userRepository.delete(id);
  }
}
