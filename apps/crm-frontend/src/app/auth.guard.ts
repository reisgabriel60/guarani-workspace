import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AppSettings } from './app-settings';

@Injectable({
  providedIn: 'root',
})
export class AuthGuard implements CanActivate {
  constructor(public router: Router) {}

  canActivate(): boolean {
    const token = localStorage.getItem(AppSettings.TOKEN_KEY);
    if (token != null && token !== '') {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }
}
