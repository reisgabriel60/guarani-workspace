import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CrmComponent } from './crm.component';

const routes: Routes = [
  {
    path: '',
    component: CrmComponent,
    children: [
      {
        path: 'assinaturas',
        loadChildren: () =>
          import('./modules/site/assinaturas/signatures.module').then(
            (m) => m.SignaturesModule,
          ),
      },
      {
        path: 'blog',
        loadChildren: () =>
          import('./modules/site/blog/blog.module').then((m) => m.BlogModule),
      },
      {
        path: 'empresas',
        loadChildren: () =>
          import('./modules/site/companies/companies.module').then(
            (m) => m.CompaniesModule,
          ),
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrmRoutingModule {}
