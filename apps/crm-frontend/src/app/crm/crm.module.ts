import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { CrmRoutingModule } from './crm-routing.module';
import { CrmComponent } from './crm.component';
import { SharedModule } from './modules/shared/shared.module';
import { SidenavWrapperModule } from './modules/sidenav-wrapper/sidenav-wrapper.module';

@NgModule({
  declarations: [CrmComponent],
  imports: [CommonModule, CrmRoutingModule, SidenavWrapperModule, SharedModule],
})
export class CrmModule {}
