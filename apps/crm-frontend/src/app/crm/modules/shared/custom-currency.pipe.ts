import { formatCurrency, getCurrencySymbol } from '@angular/common';
import { registerLocaleData } from '@angular/common';
import localeBr from '@angular/common/locales/br';
import { Pipe, PipeTransform } from '@angular/core';
registerLocaleData(localeBr, 'br');

@Pipe({
  name: 'brCurrency',
})
export class BrazilCurrencyPipe implements PipeTransform {
  transform(
    value: number,
    currencyCode: string = 'BRL',
    digitsInfo: string = '1.2',
    locale: string = 'br'
  ): string | null {
    const res: string = formatCurrency(
      value,
      locale,
      getCurrencySymbol(currencyCode, 'wide'),
      currencyCode,
      digitsInfo
    );

    return `R$ ${res.split('R$')[0]}`;
  }
}
