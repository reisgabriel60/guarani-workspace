import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

import { DialogConfirmData } from './dialog-confirm';
export { DialogConfirmData };

@Component({
  selector: 'app-dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.scss'],
})
export class DialogConfirmComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public dialogData: DialogConfirmData,
    private dialogRef: MatDialogRef<DialogConfirmComponent>,
  ) {}

  close(response: boolean) {
    this.dialogRef.close(response);
  }
}
