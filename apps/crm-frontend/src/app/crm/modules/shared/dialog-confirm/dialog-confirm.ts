export class DialogConfirmData {
  title = '';
  buttonCancel = '';
  buttonConfirm = '';
  align? = 'end';
  iconCancel?: string;
  iconConfirm?: string;
}
