export interface Action {
  name: string;
  icon: string;
  style: string;
  funcAction(row: any): void;
}
export class Actions {
  columnName = '';
  name = '';
  actions?: Array<Action>;
}
