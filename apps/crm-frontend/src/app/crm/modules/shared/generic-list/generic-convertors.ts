import { formatCurrency, getCurrencySymbol } from '@angular/common';

export class GenericConvertors {
  static convertBrlCurrency(value: any) {
    const res: string = formatCurrency(
      value,
      'br',
      getCurrencySymbol('BRL', 'wide'),
      'BRL',
      '1.2',
    );

    return `R$ ${res.split('R$')[0]}`;
  }

  static getConvertEnumFunction(TEnum: any) {
    return (valor) => TEnum[valor];
  }

  static convertDate(value) {
    if (typeof value === 'string') value = new Date(value);
    return value.toLocaleDateString();
  }
}
