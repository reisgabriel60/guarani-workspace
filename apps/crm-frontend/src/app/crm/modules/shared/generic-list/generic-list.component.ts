import { AfterViewInit, Component, Input } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Filter, FilterInteface } from '@workspace/models';

import { Actions } from './actions';
import { GenericConvertors } from './generic-convertors';
import { GenericListService } from './generic-list.service';

export { Actions };
export { GenericConvertors };

export interface IDisplayedColumns {
  name: string;
  description: string;
  getValue?(value: any): any;
}

@Component({
  selector: 'app-generic-list',
  templateUrl: './generic-list.component.html',
  styleUrls: ['./generic-list.component.scss'],
})
export class GenericListComponent implements AfterViewInit {
  @Input() displayedColumns: IDisplayedColumns[] = [
    { name: 'id', description: 'Id' },
    { name: 'name', description: 'Nome' },
    { name: 'progress', description: 'Progresso' },
    { name: 'fruit', description: 'Fruta', getValue: (value: any) => value },
  ];
  @Input() endPoint = '';
  @Input() actions?: Actions;

  paginate = {
    itemsPerPage: 5,
    currentPage: 1,
    totalItems: 0,
  };
  filter: Filter = new Filter({ value: '', column: '' });

  dataSource!: MatTableDataSource<any>;

  constructor(private genericListService: GenericListService) {}

  ngAfterViewInit(): void {
    this.pageChange(1);
  }

  @Input()
  refreshData(itemsPerPage?: number, filter?: FilterInteface) {
    if (filter) {
      this.filter = new Filter(filter);
    }
    this.paginate.itemsPerPage = itemsPerPage || 5;
    this.pageChange(1);
  }

  columnsNames(): string[] {
    const columns = this.displayedColumns.map(({ name }) => name);
    if (this.actions) {
      columns.push(this.actions.name);
    }
    return columns;
  }

  pageChange(event: number) {
    const filters: FilterInteface[] = [];
    if (this.filter && this.filter.isValid()) filters.push(this.filter);

    this.genericListService
      .getData(this.endPoint, {
        limit: this.paginate.itemsPerPage,
        filters,
        page: event,
      })
      .subscribe((response) => {
        this.dataSource = new MatTableDataSource(response.data);
        this.paginate.currentPage = event;
        this.paginate.totalItems = response.totalCount;
        window.scrollTo(0, 0);
      });
  }
}
