import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  BlogPostInterface,
  PaginatedRequestInterface,
  PaginatedResponseInterface,
} from '@workspace/models';

@Injectable({
  providedIn: 'root',
})
export class GenericListService {
  constructor(private httpClient: HttpClient) {}

  getData(url: string, paginatedRequest?: PaginatedRequestInterface) {
    const params = new HttpParams()
      .set('filters', JSON.stringify(paginatedRequest?.filters || ''))
      .set('page', paginatedRequest?.page || 1)
      .set('limit', paginatedRequest?.limit || 5);
    return this.httpClient.get<PaginatedResponseInterface<BlogPostInterface>>(
      url,
      { params },
    );
  }
}
