export class ObjectsHelper {
  static enumToArray(enumerator: any): Enumerator[] {
    return Object.keys(enumerator).map((key) => ({
      id: key,
      name: enumerator[key],
    }));
  }
}

export interface Enumerator {
  id: number | string;
  name: number | string;
}
