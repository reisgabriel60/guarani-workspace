import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTableModule } from '@angular/material/table';
import { SiteSharedModule } from '@workspace/site-shared';

import { BrazilCurrencyPipe } from './custom-currency.pipe';
import { DialogConfirmComponent } from './dialog-confirm/dialog-confirm.component';
import { GenericListComponent } from './generic-list/generic-list.component';

@NgModule({
  declarations: [
    GenericListComponent,
    DialogConfirmComponent,
    BrazilCurrencyPipe,
  ],
  exports: [GenericListComponent, BrazilCurrencyPipe],
  imports: [
    CommonModule,
    MatTableModule,
    MatFormFieldModule,
    MatPaginatorModule,
    MatInputModule,
    MatSortModule,
    MatIconModule,
    FlexModule,
    MatButtonModule,
    SiteSharedModule,
    HttpClientModule,
  ],
})
export class SharedModule {}
