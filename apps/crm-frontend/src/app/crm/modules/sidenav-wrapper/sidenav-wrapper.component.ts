import { Component } from '@angular/core';
import { Router } from '@angular/router';

import { AppSettings } from '../../../app-settings';

@Component({
  selector: 'app-sidenav-wrapper',
  templateUrl: './sidenav-wrapper.component.html',
  styleUrls: ['./sidenav-wrapper.component.scss'],
})
export class SidenavWrapperComponent {
  isExpanded = false;
  currentUser: any;
  menus = [
    { name: 'Dashboard', routerLink: 'dashboard', matIcon: 'dashboard' },
    {
      name: 'Administração',
      routerLink: '',
      matIcon: 'admin_panel_settings',
    },
    {
      name: 'Comercial',
      routerLink: '',
      matIcon: 'currency_exchange',
    },
    { name: 'Develorpers', routerLink: '', matIcon: 'developer_mode' },
    {
      name: 'Site',
      routerLink: null,
      matIcon: 'web',
      subListExpanded: false,
      subList: [
        {
          name: 'Assinaturas',
          routerLink: 'assinaturas',
          matIcon: 'price_change',
        },
        { name: 'Blog', routerLink: 'blog', matIcon: 'newspaper' },
        { name: 'Empresas', routerLink: 'empresas', matIcon: 'business' },
        { name: 'iBR Números', routerLink: '', matIcon: 'numbers' },
        {
          name: 'Perguntas Frequentes',
          routerLink: '',
          matIcon: 'quiz',
        },
        {
          name: 'Principais Funções',
          routerLink: '',
          matIcon: 'engineering',
        },
        {
          name: 'Produtos',
          routerLink: '',
          matIcon: 'production_quantity_limits',
        },
        { name: 'Setores', routerLink: '', matIcon: 'category' },
        { name: 'Segmentos', routerLink: '', matIcon: 'segment' },
        { name: 'Store', routerLink: '', matIcon: 'store' },
        { name: 'Vagas', routerLink: '', matIcon: 'work' },
      ],
    },
    { name: 'Suporte', routerLink: '', matIcon: 'support_agent' },
    { name: 'Configurações', routerLink: '', matIcon: 'settings' },
  ];

  constructor(private route: Router) {
    this.currentUser = localStorage.getItem('currentUser');
  }

  logOut() {
    localStorage.removeItem(AppSettings.TOKEN_KEY);
    this.route.navigateByUrl('/login');
  }
}
