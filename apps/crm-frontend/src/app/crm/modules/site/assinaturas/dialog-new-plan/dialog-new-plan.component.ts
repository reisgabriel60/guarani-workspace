import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatTableDataSource } from '@angular/material/table';

import { IFunc } from '../interfaces/IFunc';
import { IPlan } from '../interfaces/IPlan';

@Component({
  selector: 'app-dialog-new-plan',
  templateUrl: './dialog-new-plan.component.html',
  styleUrls: ['./dialog-new-plan.component.scss'],
})
export class DialogNewPlanComponent {
  model: IPlan = {
    id: 0,
    product: '',
    charge: '',
    value: 0,
    plans: { titles: [], functions: [] },
  };
  dataSource = new MatTableDataSource();
  displayedColumns = ['func'];
  selectedFunctions: string[] = [];
  functions: IFunc[] = [
    { name: 'Estoque', id: 0 },
    { name: 'NF-e Ilimitado', id: 1 },
    { name: 'Compras', id: 2 },
    { name: 'Vendas', id: 3 },
    { name: 'Cadastro em Massa', id: 4 },
    { name: 'Clientes', id: 5 },
    { name: 'Fornecedores', id: 6 },
    { name: 'Transportadora', id: 7 },
    { name: 'Vendedores', id: 8 },
    { name: 'Suporte Técnico', id: 9 },
    { name: 'Resumo', id: 10 },
    { name: 'NFC-e Ilimitado', id: 11 },
    { name: 'Etiquetas', id: 12 },
    { name: 'Contas a Pagar', id: 13 },
    { name: 'Contas a Receber', id: 14 },
    { name: 'Int. Mercado Livre', id: 15 },
    { name: 'Usuários', id: 16 },
  ];

  constructor(@Inject(MAT_DIALOG_DATA) public data: IPlan) {
    if (data) {
      this.model = data;
      this.selectedFunctions = this.model.plans.functions.map(
        ({ func }) => func.name,
      );
      this.refreshList();
      if (this.model.plans.titles && this.model.plans.titles.length > 0) {
        this.displayedColumns = [
          'func',
          ...Array(this.model.plans.titles.length)
            .fill('level')
            .map((value, index) => value + index),
        ];
      }
    }
  }

  refreshList() {
    this.dataSource.data = this.model.plans.functions;
    console.log(this.model.plans.functions);
  }

  newPlan() {
    this.displayedColumns = [
      'func',
      ...Array(this.model.plans.titles.length)
        .fill('level')
        .map((value, index) => value + index),
    ];
  }

  compareObjects(o1: any, o2: any) {
    return o1.name == o2.name && o1.id == o2.id;
  }
}
