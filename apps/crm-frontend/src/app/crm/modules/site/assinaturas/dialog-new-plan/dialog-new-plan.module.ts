import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatChipsModule } from '@angular/material/chips';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import {
  CurrencyMaskConfig,
  CurrencyMaskInputMode,
  NgxCurrencyModule,
} from 'ngx-currency';

import { DialogNewPlanComponent } from './dialog-new-plan.component';

export const currencyMaskConfig: CurrencyMaskConfig = {
  prefix: 'R$ ',
  thousands: '.',
  decimal: ',',
  align: 'left',
  allowNegative: true,
  allowZero: true,
  precision: 2,
  suffix: '',
  nullable: true,
  min: undefined,
  max: undefined,
  inputMode: CurrencyMaskInputMode.FINANCIAL,
};

@NgModule({
  declarations: [DialogNewPlanComponent],
  imports: [
    CommonModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    MatIconModule,
    FlexModule,
    MatButtonModule,
    MatInputModule,
    MatChipsModule,
    MatSelectModule,
    MatAutocompleteModule,
    ReactiveFormsModule,
    NgxCurrencyModule.forRoot(currencyMaskConfig),
    MatTableModule,
  ],
  exports: [DialogNewPlanComponent],
})
export class DialogNewPlanModule {}
