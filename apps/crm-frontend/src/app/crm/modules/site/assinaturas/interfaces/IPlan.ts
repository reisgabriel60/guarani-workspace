export interface IPlan {
  id: number;
  product: string;
  charge: string;
  value: number;
  plans: {
    titles: Array<string>;
    functions: Array<{
      func: { id: number; name: string };
      level0?: boolean | string;
      level1?: boolean | string;
      level2?: boolean | string;
      level3?: boolean | string;
      level4?: boolean | string;
      level5?: boolean | string;
      level6?: boolean | string;
    }>;
  };
}
