import { Component } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import {
  DialogConfirmComponent,
  DialogConfirmData,
} from '../../shared/dialog-confirm/dialog-confirm.component';
import { Actions } from '../../shared/generic-list/actions';
import { GenericConvertors } from '../../shared/generic-list/generic-convertors';
import { DialogNewPlanComponent } from './dialog-new-plan/dialog-new-plan.component';
import { ChargingFrequencyEnum } from './interfaces/charging-frequency.enum';
import { IPlan } from './interfaces/IPlan';

@Component({
  selector: 'app-signatures',
  templateUrl: './signatures.component.html',
  styleUrls: ['./signatures.component.scss'],
})
export class SignaturesComponent {
  displayedColumns = [
    { name: 'product', description: 'Produto' },
    {
      name: 'charge',
      description: 'Cobrança',
      getValue: GenericConvertors.getConvertEnumFunction(ChargingFrequencyEnum),
    },
    {
      name: 'valor',
      description: 'Valor',
      getValue: GenericConvertors.convertBrlCurrency,
    },
  ];
  actions: Actions;
  data: IPlan[] = [
    {
      id: 1,
      product: 'Gestão',
      charge: 'M',
      value: 89.9,
      plans: {
        titles: ['Bronse'],
        functions: [{ func: { name: 'Estoque', id: 1 }, level0: true }],
      },
    },
    {
      id: 2,
      product: 'Desmonte',
      charge: 'A',
      value: 59.9,
      plans: { titles: [], functions: [] },
    },
    {
      id: 3,
      product: 'CDV',
      charge: 'A',
      value: 129.9,
      plans: { titles: [], functions: [] },
    },
  ];

  constructor(private dialog: MatDialog) {
    this.actions = {
      columnName: 'Ações',
      name: 'acoes',
      actions: [
        {
          name: 'preview',
          icon: 'visibility',
          style: 'color: #ce6200',
          funcAction: () => this.openNewPlan(),
        },
        {
          name: 'edit',
          icon: 'edit_square',
          style: 'color: #624de3',
          funcAction: (row: any) => this.openNewPlan(row),
        },
        {
          name: 'delete',
          icon: 'delete',
          style: 'color: #a30d11',
          funcAction: (row: any) => this.removePlan(row),
        },
      ],
    };
  }

  removePlan(row: any) {
    const data = new DialogConfirmData();
    data.title = 'Tem certeza que deseja excluir esse plano?';
    data.buttonCancel = 'Cancelar';
    data.buttonConfirm = 'Excluir';
    data.iconCancel = 'close';
    data.iconConfirm = 'delete_forever';
    this.dialog
      .open(DialogConfirmComponent, {
        data,
        panelClass: 'dialog-container-rounded',
      })
      .afterClosed()
      .subscribe((response) => {
        if (response) {
          this.data = this.data.filter((data) => data !== row);
        }
      });
  }

  openNewPlan(row?: any) {
    this.dialog
      .open(DialogNewPlanComponent, {
        panelClass: 'dialog-container-rounded',
        data: row,
      })
      .afterClosed()
      .subscribe((response) => {
        if (response) {
          if (!row) {
            this.data.push(response);
          } else {
            const index = this.data.findIndex((plan) => plan === row);
            if (index !== -1) {
              this.data[index] = response;
            }
          }
          this.data = [...this.data];
        }
      });
  }
}
