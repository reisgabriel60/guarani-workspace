import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';

import { SharedModule } from '../../shared/shared.module';
import { DialogNewPlanModule } from './dialog-new-plan/dialog-new-plan.module';
import { SignaturesRoutingModule } from './signatures-routing.module';
import { SignaturesComponent } from './signatures.component';

@NgModule({
  declarations: [SignaturesComponent],
  imports: [
    CommonModule,
    SignaturesRoutingModule,
    SharedModule,
    MatIconModule,
    FlexModule,
    MatButtonModule,
    DialogNewPlanModule,
  ],
})
export class SignaturesModule {}
