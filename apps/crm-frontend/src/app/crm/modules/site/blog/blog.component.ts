import { Component, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { BlogPostInterface, FilterInteface } from '@workspace/models';

import { environment } from '../../../../../environments/environment';
import { DialogConfirmData } from '../../shared/dialog-confirm/dialog-confirm';
import { DialogConfirmComponent } from '../../shared/dialog-confirm/dialog-confirm.component';
import {
  Actions,
  GenericConvertors,
  GenericListComponent,
} from '../../shared/generic-list/generic-list.component';
import { BlogService } from './blog.service';
import { DialogNewPostComponent } from './dialog-new-post/dialog-new-post.component';
import { DialogPreviewPostComponent } from './dialog-preview-post/dialog-preview-post.component';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.scss'],
})
export class BlogComponent {
  displayedColumns = [
    { name: 'title', description: 'Titulo' },
    {
      name: 'createdAt',
      description: 'Publicação',
      getValue: GenericConvertors.convertDate,
    },
  ];

  actions: Actions;
  data: BlogPostInterface[] = [];
  endPoint = environment.urlCrmApi + '/post';
  showing = 5;
  filter: FilterInteface = {
    column: 'title',
    value: '',
    filterType: 'contem',
  };

  @ViewChild('listSignatures') listSignatures!: GenericListComponent;

  constructor(private dialog: MatDialog, private blogService: BlogService) {
    this.actions = {
      columnName: 'Ações',
      name: 'acoes',
      actions: [
        {
          name: 'preview',
          icon: 'visibility',
          style: 'color: #ce6200',
          funcAction: (row: any) => this.openPreviewPost(row),
        },
        {
          name: 'edit',
          icon: 'edit_square',
          style: 'color: #624de3',
          funcAction: (row: any) => this.openNewPost(row),
        },
        {
          name: 'delete',
          icon: 'delete',
          style: 'color: #a30d11',
          funcAction: (row: any) => this.removePost(row),
        },
      ],
    };
  }

  removePost(row: any) {
    const data = new DialogConfirmData();
    data.title = 'Tem certeza que deseja excluir essa Notícia?';
    data.buttonCancel = 'Cancelar';
    data.buttonConfirm = 'Excluir';
    data.iconCancel = 'close';
    data.iconConfirm = 'delete_forever';
    this.dialog
      .open(DialogConfirmComponent, {
        data,
        panelClass: 'dialog-container-rounded',
      })
      .afterClosed()
      .subscribe((response) => {
        if (response) {
          this.blogService
            .deletePost(row.id)
            .subscribe(() =>
              this.listSignatures.refreshData(this.showing, this.filter),
            );
        }
      });
  }

  openNewPost(row?: any) {
    this.dialog
      .open(DialogNewPostComponent, {
        panelClass: 'dialog-container-rounded',
        data: row,
      })
      .afterClosed()
      .subscribe(() =>
        this.listSignatures.refreshData(this.showing, this.filter),
      );
  }

  openPreviewPost(row: BlogPostInterface) {
    this.dialog
      .open(DialogPreviewPostComponent, {
        data: row,
        panelClass: 'dialog-container-rounded',
      })
      .afterClosed()
      .subscribe();
  }
}
