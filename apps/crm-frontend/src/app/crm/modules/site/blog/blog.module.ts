import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTableModule } from '@angular/material/table';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { SiteSharedModule } from '@workspace/site-shared';

import { InterceptorModule } from '../../../../interceptor/interceptor.module';
import { SharedModule } from '../../shared/shared.module';
import { BlogRoutingModule } from './blog-routing.module';
import { BlogComponent } from './blog.component';
import { BlogService } from './blog.service';
import { DialogNewPostComponent } from './dialog-new-post/dialog-new-post.component';
import { DialogPreviewPostComponent } from './dialog-preview-post/dialog-preview-post.component';

@NgModule({
  declarations: [
    BlogComponent,
    DialogNewPostComponent,
    DialogPreviewPostComponent,
  ],
  imports: [
    CommonModule,
    BlogRoutingModule,
    SharedModule,
    MatIconModule,
    FlexModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    AngularEditorModule,
    HttpClientModule,
    MatCheckboxModule,
    SiteSharedModule,
    InterceptorModule,
    MatTableModule,
  ],
  providers: [BlogService],
})
export class BlogModule {}
