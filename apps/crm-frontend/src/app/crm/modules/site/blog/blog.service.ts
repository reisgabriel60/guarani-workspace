import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class BlogService {
  constructor(private httpClient: HttpClient) {}

  savePost(modelPost) {
    let method = 'post';
    let path = '/post';
    if (modelPost.id) {
      method = 'patch';
      path = `/post/${modelPost.id}`;
    }

    delete modelPost.createdAt;
    delete modelPost.updatedAt;
    delete modelPost.slug;
    delete modelPost.id;
    if (!(modelPost.image instanceof File)) {
      delete modelPost.image;
    }

    const formData = new FormData();
    Object.keys(modelPost).forEach((key) =>
      formData.append(key, modelPost[key]),
    );

    return this.httpClient.request(method, environment.urlCrmApi + path, {
      body: formData,
    });
  }

  deletePost(id: string) {
    return this.httpClient.delete(environment.urlCrmApi + `/post/${id}`);
  }
}
