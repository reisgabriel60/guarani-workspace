import { Component, Inject, OnInit } from '@angular/core';
import {
  MAT_DIALOG_DATA,
  MatDialog,
  MatDialogRef,
} from '@angular/material/dialog';
import { AngularEditorConfig } from '@kolkov/angular-editor';
import { BlogPost, BlogPostInterface, ImageInterface } from '@workspace/models';
import { NotifyService } from '@workspace/site-shared';

import { BlogService } from '../blog.service';
import { DialogPreviewPostComponent } from '../dialog-preview-post/dialog-preview-post.component';

@Component({
  selector: 'app-dialog-new-post',
  templateUrl: './dialog-new-post.component.html',
  styleUrls: ['./dialog-new-post.component.scss'],
})
export class DialogNewPostComponent implements OnInit {
  model = new BlogPost();

  editorConfig: AngularEditorConfig = {
    editable: true,
    spellcheck: true,
    height: 'auto',
    minHeight: '100px',
    maxHeight: 'auto',
    width: 'auto',
    minWidth: '0',
    translate: 'yes',
    enableToolbar: true,
    showToolbar: true,
    placeholder: 'Escreva o Texto Aqui ...',
    fonts: [
      { class: 'arial', name: 'Arial' },
      { class: 'times-new-roman', name: 'Times New Roman' },
      { class: 'calibri', name: 'Calibri' },
      { class: 'comic-sans-ms', name: 'Comic Sans MS' },
    ],
    sanitize: false,
    toolbarPosition: 'top',
    toolbarHiddenButtons: [['insertVideo']],
  };
  selectedImage = '#';

  constructor(
    @Inject(MAT_DIALOG_DATA) public data: BlogPostInterface,
    private dialogRef: MatDialogRef<DialogNewPostComponent>,
    private dialog: MatDialog,
    private blogService: BlogService,
    private notifyService: NotifyService,
  ) {
    if (data) {
      Object.assign(this.model, data);
      this.selectedImage = (this.model.image as ImageInterface)?.url || '#';
    }
  }

  ngOnInit(): void {
    //
  }

  showImg(file) {
    this.getBase64(file).then(
      (base64: string) => (this.selectedImage = base64),
    );
  }

  save() {
    if (!this.model.image) {
      this.notifyService.warn('É necessario específicar uma imagem.');
      return;
    }
    this.blogService.savePost(this.model).subscribe({
      next: () => {
        this.dialogRef.close(this.model);
      },
      error: ({ error }) => this.notifyService.alert(error.error?.message[0]),
    });
  }

  getBase64(file) {
    return new Promise<string>((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(String(reader.result));
      reader.onerror = (error) => reject(error);
    });
  }

  openPreviewPost() {
    const data = this.model;

    this.dialog
      .open(DialogPreviewPostComponent, {
        data,
        panelClass: 'dialog-container-rounded',
      })
      .afterClosed()
      .subscribe();
  }

  changeImage(inputImg: HTMLInputElement) {
    if (inputImg.value && inputImg.files && inputImg.files.length > 0) {
      this.model.image = inputImg.files[0] as any;
      this.showImg(this.model.image);
    }
  }
}
