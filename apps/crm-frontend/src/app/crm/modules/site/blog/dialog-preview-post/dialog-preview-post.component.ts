import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { BlogPostInterface } from '@workspace/models';

@Component({
  selector: 'app-dialog-preview-post',
  templateUrl: './dialog-preview-post.component.html',
  styleUrls: ['./dialog-preview-post.component.scss'],
})
export class DialogPreviewPostComponent {
  model: BlogPostInterface;
  constructor(@Inject(MAT_DIALOG_DATA) public data: BlogPostInterface) {
    this.model = data;
  }
}
