import { Component } from '@angular/core';
import { Company, CompanyInterface } from '@workspace/models';

import { CompaniesService } from './companies.service';

@Component({
  selector: 'app-companies',
  templateUrl: './companies.component.html',
  styleUrls: ['./companies.component.scss'],
})
export class CompaniesComponent {
  companies: CompanyInterface[][] = [
    Array(5).fill(new Company()),
    Array(5).fill(new Company()),
  ];

  constructor(private companiesService: CompaniesService) {
    this.companiesService.getCompanies().subscribe((response) => {
      response.data.forEach((company) => {
        this.companies[company.positionY][company.positionX] = company;
      });
    });
  }

  changeImage(x, y, image: HTMLInputElement) {
    if (image.value && image.files && image.files.length > 0) {
      let company = this.companies[y][x];
      if (!company.id) {
        company = new Company();
        company.positionX = x;
        company.positionY = y;
        this.companies[y][x] = company;
      }
      company.image = image.files[0];
      this.companiesService.saveCompany(company).subscribe((response) => {
        Object.assign(company, response);
      });
    }
  }

  delete(col: CompanyInterface) {
    if (col.id) {
      this.companiesService
        .deleteCompany(col.id)
        .subscribe(
          () => (this.companies[col.positionY][col.positionX] = new Company()),
        );
    }
  }
}
