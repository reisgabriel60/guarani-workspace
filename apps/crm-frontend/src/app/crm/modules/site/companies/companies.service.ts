import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {
  CompanyInterface,
  PaginatedResponseInterface,
} from '@workspace/models';

import { environment } from '../../../../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CompaniesService {
  constructor(private httpClient: HttpClient) {}

  getCompanies() {
    return this.httpClient.get<PaginatedResponseInterface<CompanyInterface>>(
      environment.urlCrmApi + '/companies',
    );
  }

  saveCompany(companyElement: CompanyInterface) {
    delete companyElement.createdAt;
    delete companyElement.updatedAt;
    const formData = new FormData();
    Object.keys(companyElement).forEach((key) =>
      formData.append(key, companyElement[key]),
    );
    if (!companyElement.id) {
      return this.httpClient.post(
        environment.urlCrmApi + '/companies',
        formData,
      );
    } else {
      formData.delete('id');
      return this.httpClient.patch(
        environment.urlCrmApi + `/companies/${companyElement.id}`,
        formData,
      );
    }
  }

  deleteCompany(id: string) {
    return this.httpClient.delete(environment.urlCrmApi + `/companies/${id}`);
  }
}
