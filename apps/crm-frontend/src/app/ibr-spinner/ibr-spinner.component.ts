import { Component, Injector } from '@angular/core';
import { Observable } from 'rxjs';

import { IbrSpinnerService } from './ibr-spinner.service';
@Component({
  template: `
    <div *ngIf="loading | async" class="spinner-container">
      <img
        src="../../assets/images/ibr-spinner.gif"
        alt="ibr-spinner"
        width="300px"
        height="300px" />
    </div>
  `,
  styles: [
    `
      .spinner-container {
        width: 100%;
        height: 100vh;
        display: flex;
        background-color: #000000e0;
        justify-content: center;
        align-items: center;
        position: absolute;
        z-index: 1001;
      }
    `,
  ],
  selector: 'app-ibr-spinner',
})
export class IbrSpinnerComponent {
  loading: Observable<boolean> = new Observable<boolean>();

  constructor(
    public injector: Injector,
    private ibrSpinnerService: IbrSpinnerService,
  ) {
    this.setLoading();
  }

  setLoading() {
    this.loading = this.ibrSpinnerService.getLoading();
  }
}
