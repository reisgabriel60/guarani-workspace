import {
  HttpErrorResponse,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { AppSettings } from '../app-settings';

@Injectable()
export class Interceptor implements HttpInterceptor {
  constructor(private router: Router) {}

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    const redirectFrom = window.location.pathname;
    if (
      !req.url.includes('/files') &&
      localStorage.getItem(AppSettings.TOKEN_KEY)
    ) {
      const token = localStorage.getItem(AppSettings.TOKEN_KEY);
      const decodedToken = JSON.parse(atob((token || '').split('.')[1]));
      if (Date.now() > decodedToken.exp * 1000) {
        alert(
          'Sessão expirou ou é iválida.\n' +
            ' Realize o login para acessar este recurso',
        );
        this.router.navigateByUrl('/login?redirectFrom=' + redirectFrom);
        localStorage.removeItem(AppSettings.TOKEN_KEY);
        return next.handle(req);
      }
      const authReq = req.clone({
        setHeaders: { Authorization: `Bearer ${token}` },
      });

      return next.handle(authReq).pipe(
        catchError((error) => {
          if (error instanceof HttpErrorResponse) {
            if (error.status === 401) {
              alert(
                'Sessão expirou ou é iválida.\n' +
                  ' Realize o login para acessar este recurso',
              );
              this.router.navigateByUrl('/login?redirectFrom=' + redirectFrom);
              localStorage.removeItem(AppSettings.TOKEN_KEY);
            } else if (error.status === 0) {
              alert('Verifique sua conexão');
            } else if (error.status === 403) {
              alert(
                'Atenção!\n' +
                  'Você não possui permissão para acessar este recurso!',
              );
              this.router.navigate(['']);
            } else if (error.status === 500) {
              alert(
                'Erro no servidor interno!\n' +
                  'Tente atualizar a página. Caso o problema persista entre em contato com o nosso Suporte.',
              );
            }
          }
          return throwError(error);
        }),
      );
    }
    return next.handle(req);
  }
}
