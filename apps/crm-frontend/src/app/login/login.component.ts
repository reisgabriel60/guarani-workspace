import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { NotifyService } from '@workspace/site-shared';

import { IbrSpinnerService } from '../ibr-spinner/ibr-spinner.service';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  model = { email: '', password: '' };
  TOKEN_KEY = 'access_token';

  constructor(
    private route: Router,
    private ibrSpinnerService: IbrSpinnerService,
    private notifyService: NotifyService,
    private loginService: LoginService,
  ) {}

  login() {
    this.ibrSpinnerService.show();
    this.loginService.login(this.model).subscribe({
      next: (response: any) => {
        if (response.access_token) {
          localStorage.setItem(this.TOKEN_KEY, response.access_token);
          this.route.navigateByUrl('');
          this.notifyService.success('Bem vindo!');
        } else {
          this.notifyService.warn(
            'Não foi possivel realizar login com esas credenciais',
          );
        }
        this.ibrSpinnerService.hide();
      },
      error: () => {
        this.ibrSpinnerService.hide();
        this.notifyService.warn(
          'Não foi possivel realizar login com esas credenciais',
        );
      },
    });
  }
}
