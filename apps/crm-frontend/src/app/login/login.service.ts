import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  BASE_URL = environment.urlCrmApi;
  constructor(private httpClient: HttpClient) {}

  login(model) {
    const url = this.BASE_URL + '/auth/login';
    return this.httpClient.post(url, model, { observe: 'body' });
  }
}
