import { Component } from '@angular/core';

import { routes } from '../constants/general-constants';
import { AppService } from './app.service';

@Component({
  selector: 'guarani-workspace-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  routes = routes;
  constructor(private appService: AppService) {}

  onScroll(event: any) {
    if (
      event.target.offsetHeight + event.target.scrollTop + 10 >=
      event.target.scrollHeight
    ) {
      console.log('ScrollEnd');
      this.appService.scrollEndPageEvent.emit();
    }
  }
}
