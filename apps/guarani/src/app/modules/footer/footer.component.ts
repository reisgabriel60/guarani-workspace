import { Component, ViewEncapsulation } from '@angular/core';

export interface ISocialIcons {
  href: string;
  src: string;
  alt: string;
}

@Component({
  selector: 'guarani-workspace-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class FooterComponent {
  socialIcons: ISocialIcons[] = [
    {
      href: 'https://www.facebook.com/ibrtecnologialtda',
      src: '/assets/social-icons/icon-facebook.png',
      alt: 'Icone Facebook',
    },
    {
      href: 'https://www.instagram.com/ibrtecnologia/',
      src: '/assets/social-icons/icon-insta.png',
      alt: 'Icone Instagram',
    },
    {
      href: 'https://www.linkedin.com/company/ibr-tecnologia/',
      src: '/assets/social-icons/icon-likedin.png',
      alt: 'Icone Linkedin',
    },
    {
      href: 'https://api.whatsapp.com/send?phone=5555997195918&text=Olá, vim pelo site, gostaria de uma demosntração gratuita.',
      src: '/assets/social-icons/icon-whats.png',
      alt: 'Icone WhatsApp',
    },
    {
      href: 'https://www.youtube.com/channel/UCKyKfRYWoCT305SYYYvsrsQ/featured',
      src: '/assets/social-icons/icon-youtube.png',
      alt: 'Icone Youtube',
    },
  ];
}
