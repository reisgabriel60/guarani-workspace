import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'guarani-workspace-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class GalleryComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
