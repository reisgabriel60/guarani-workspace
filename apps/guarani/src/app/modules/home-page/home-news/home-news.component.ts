import { HttpClient } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {
  BlogPostInterface,
  PaginatedResponseInterface,
} from '@workspace/models';

import { routes } from '../../../../constants/general-constants';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'guarani-workspace-home-news',
  templateUrl: './home-news.component.html',
  styleUrls: ['./home-news.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class HomeNewsComponent implements OnInit {
  newsPagePath = routes.newsPagePath;
  newsArray!: BlogPostInterface[];
  constructor(private httpClient: HttpClient) {}

  ngOnInit(): void {
    this.httpClient
      .get<PaginatedResponseInterface<BlogPostInterface>>(
        `${environment.urlCrmApi}/post`,
      )
      .subscribe((value) => (this.newsArray = value.data));
  }
}
