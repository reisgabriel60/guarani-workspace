import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';

import { GalleryComponent } from './gallery/gallery.component';
import { HomeNewsComponent } from './home-news/home-news.component';
import { HomePageRoutingModule } from './home-page-routing.module';
import { HomePageComponent } from './home-page.component';
import { SponsorsComponent } from './sponsors/sponsors.component';
import { StoreComponent } from './store/store.component';

@NgModule({
  declarations: [
    HomePageComponent,
    GalleryComponent,
    HomeNewsComponent,
    StoreComponent,
    SponsorsComponent,
  ],
  imports: [
    CommonModule,
    HomePageRoutingModule,
    FlexModule,
    MatFormFieldModule,
    MatIconModule,
  ],
})
export class HomePageModule {}
