import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'guarani-workspace-sponsors',
  templateUrl: './sponsors.component.html',
  styleUrls: ['./sponsors.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class SponsorsComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
