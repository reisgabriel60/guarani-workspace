import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'guarani-workspace-store',
  templateUrl: './store.component.html',
  styleUrls: ['./store.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class StoreComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
