import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NewsPageComponent } from './news-page.component';
import { NewsComponent } from './news/news.component';

const routes: Routes = [
  { path: '', component: NewsPageComponent },
  { path: ':slug', component: NewsComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NewsPageRoutingModule {}
