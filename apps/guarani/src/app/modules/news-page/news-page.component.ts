import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import {
  BlogPostInterface,
  PaginatedResponseInterface,
} from '@workspace/models';
import { debounceTime } from 'rxjs';

import { routes } from '../../../constants/general-constants';
import { environment } from '../../../environments/environment';
import { AppService } from '../../app.service';

@Component({
  selector: 'guarani-workspace-home-news-page',
  templateUrl: './news-page.component.html',
  styleUrls: ['./news-page.component.scss'],
})
export class NewsPageComponent implements OnInit {
  routes = routes;
  newsArray: BlogPostInterface[] = [];
  totalCount!: number;
  constructor(private httpClient: HttpClient, private appService: AppService) {}

  ngOnInit(): void {
    this.getNews(1, 8);
    this.appService.scrollEndPageEvent.pipe(debounceTime(300)).subscribe(() => {
      if (this.newsArray.length < this.totalCount) {
        this.getNews(Math.trunc(this.newsArray.length / 4) + 1, 4);
      }
    });
  }

  private getNews(page: number, limit: number) {
    this.httpClient
      .get<PaginatedResponseInterface<BlogPostInterface>>(
        `${environment.urlCrmApi}/post?page=${page}&limit=${limit}`,
      )
      .subscribe((value) => {
        this.newsArray.push(...value.data);
        this.totalCount = value.totalCount;
      });
  }
}
