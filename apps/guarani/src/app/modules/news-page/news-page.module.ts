import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexModule } from '@angular/flex-layout';
import { SiteSharedModule } from '@workspace/site-shared';

import { NewsPageRoutingModule } from './news-page-routing.module';
import { NewsPageComponent } from './news-page.component';
import { NewsComponent } from './news/news.component';

@NgModule({
  declarations: [NewsPageComponent, NewsComponent],
  imports: [CommonModule, NewsPageRoutingModule, SiteSharedModule, FlexModule],
  exports: [NewsPageComponent],
})
export class NewsPageModule {}
