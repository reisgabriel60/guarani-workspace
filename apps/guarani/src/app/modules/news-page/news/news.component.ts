import { HttpClient, HttpParams } from '@angular/common/http';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {
  BlogPostInterface,
  Filter,
  FilterInteface,
  PaginatedResponseInterface,
} from '@workspace/models';
import { Observable } from 'rxjs';

import { environment } from '../../../../environments/environment';

@Component({
  selector: 'guarani-workspace-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.scss'],
  encapsulation: ViewEncapsulation.Emulated,
})
export class NewsComponent implements OnInit {
  model: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private httpClient: HttpClient,
  ) {}

  ngOnInit(): void {
    this.activatedRoute.paramMap.subscribe((params) => {
      const slug = params.get('slug');
      if (slug) {
        this.getPostBySlug(slug).subscribe((response: any) => {
          if (response && response.data.length > 0) {
            this.model = response.data[0];
          }
        });
      }
    });
  }
  getPostBySlug(
    slug: string,
  ): Observable<PaginatedResponseInterface<BlogPostInterface>> {
    const filters: FilterInteface[] = [];
    const slugFilter = new Filter({
      column: 'slug',
      value: slug,
      filterType: '==',
    });
    if (slugFilter.isValid()) filters.push(slugFilter);

    return this.httpClient.get<PaginatedResponseInterface<BlogPostInterface>>(
      environment.urlCrmApi + '/post',
      {
        params: new HttpParams().set('filters', JSON.stringify(filters)),
      },
    );
  }
}
