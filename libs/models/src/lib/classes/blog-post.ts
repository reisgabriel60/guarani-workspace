import { BlogPostInterface } from '../interfaces/blog-post.interface';
import { ImageInterface } from '../interfaces/image.interface';

export class BlogPost implements BlogPostInterface {
  id? = '';
  title = '';
  slug = '';
  content = '';
  img!: string | File | null;
  image!: ImageInterface;
  isMainBanner = false;
  createdAt!: Date;
}
