import { CompanyInterface } from '@workspace/models';

export class Company implements CompanyInterface {
  image = '';
  positionX = 0;
  positionY = 0;
}
