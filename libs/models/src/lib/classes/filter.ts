import { FilterInteface, FilterType } from '../interfaces/filter.interface';

export class Filter implements FilterInteface {
  column: string;
  value: string;
  filterType?: FilterType;

  constructor(filter: FilterInteface) {
    this.column = filter.column;
    this.value = filter.value;
    this.filterType = filter.filterType;
  }

  isValid() {
    return !!this.column && !!this.value && !!this.filterType;
  }
}
