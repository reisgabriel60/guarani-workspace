import { FotoInterface } from './foto.interface';

export interface AlbumInterface {
  id?: string;
  fotos: FotoInterface[];
  createdAt: Date;
  updatedAt?: Date;
}
