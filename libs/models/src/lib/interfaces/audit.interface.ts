export interface AuditInterface {
  id?: string;
  createdAt?: Date;
  methodName: string;
  modelName: string;
  oldObject?: string;
  newObject?: string;
}
