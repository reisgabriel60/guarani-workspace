import { ImageInterface } from './image.interface';

export interface BlogPostInterface {
  id?: string;
  title: string;
  slug: string;
  content: string;
  image: ImageInterface;
  isMainBanner: boolean;
  createdAt: Date;
}
