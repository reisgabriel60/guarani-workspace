export interface CompanyInterface {
  id?: string;
  image: any;
  positionY: number;
  positionX: number;
  createdAt?: Date;
  updatedAt?: Date;
}
