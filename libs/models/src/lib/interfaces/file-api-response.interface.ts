export interface FileApiResponseInterface {
  id: number;
  file: string;
  path: string;
  created_at: string;
  updated_at: string;
}
