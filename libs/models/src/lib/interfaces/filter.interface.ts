export type FilterType = '==' | '<=' | '>=' | '!=' | 'contem';

export interface FilterInteface {
  column: string;
  value: string;
  filterType?: FilterType;
}
