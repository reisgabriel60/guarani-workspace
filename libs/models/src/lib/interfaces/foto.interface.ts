import { ImageInterface } from '@workspace/models';

import { AlbumInterface } from './album.interface';

export interface FotoInterface {
  id?: string;
  album: AlbumInterface;
  image: ImageInterface;
}
