export interface ImageInterface {
  id: string;
  key: string;
  url: string;
  ETag: string;
  imageSize: number;
  bucketName: string;
  createdAt: Date;
  updatedAt?: Date;
}
