import { FilterInteface } from './filter.interface';

export interface PaginatedRequestInterface {
  filters?: FilterInteface[];
  page?: number;
  limit?: number;
}
