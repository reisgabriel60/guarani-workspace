export interface PaginatedResponseInterface<T> {
  data: T[];
  totalCount: number;
}
