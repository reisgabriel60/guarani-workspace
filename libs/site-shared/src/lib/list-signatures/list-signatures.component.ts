import { Component, Input } from '@angular/core';
import { SwiperOptions } from 'swiper';

@Component({
  selector: 'workspace-lista-assinaturas',
  templateUrl: './list-signatures.component.html',
  styleUrls: ['./list-signatures.component.scss'],
})
export class ListSignaturesComponent {
  config: SwiperOptions = {
    observer: true,
    observeParents: true,
    navigation: {
      nextEl: '.swiper-button-next',
      prevEl: '.swiper-button-prev',
    },
    slidesPerView: 'auto',
    loopFillGroupWithBlank: true,
    spaceBetween: 30,
  };

  model = {
    buttonToggleModule: 'anual',
  };

  @Input() plans: any;

  getClass(levelDescription: boolean, dataElement: boolean | string) {
    if (levelDescription) {
      return 'card-conteudo';
    } else if (typeof dataElement === 'boolean') {
      if (dataElement) {
        return 'card-conteudo numeros-niveis verde';
      } else {
        return `card-conteudo numeros-niveis vermelho`;
      }
    } else {
      return `card-conteudo numeros-niveis`;
    }
  }

  typeOf(dataElement: any) {
    return typeof dataElement;
  }

  getBackground(level: any) {
    if (level.backgroundManutencao) {
      return 'backgroundManutencao card-nivel';
    } else if (level.backgroundCondicoesEspeciais) {
      return 'backgroundCondicoesEspeciais card-nivel';
    } else {
      return 'card-nivel';
    }
  }
}
