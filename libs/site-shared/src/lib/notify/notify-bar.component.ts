import { Component, Inject } from '@angular/core';
import { MAT_SNACK_BAR_DATA } from '@angular/material/snack-bar';

@Component({
  selector: 'app-notify-bar',
  templateUrl: 'notify-bar.component.html',
  styleUrls: ['notify-bar.component.css'],
})
export class NotifyComponent {
  constructor(@Inject(MAT_SNACK_BAR_DATA) public data: string) {}
}
