import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { NotifyComponent } from './notify-bar.component';

@Injectable({
  providedIn: 'root',
})
export class NotifyService {
  constructor(private _snackBar: MatSnackBar) {}

  success(msg: string) {
    this._snackBar.openFromComponent(NotifyComponent, {
      duration: 3000,
      data: msg,
      panelClass: ['bg-success'],
      horizontalPosition: 'right',
    });
  }

  alert(msg: string) {
    this._snackBar.openFromComponent(NotifyComponent, {
      duration: 3000,
      data: msg,
      panelClass: ['bg-danger'],
      horizontalPosition: 'right',
    });
  }

  info(msg: string) {
    this._snackBar.openFromComponent(NotifyComponent, {
      duration: 3000,
      data: msg,
      panelClass: ['bg-info'],
      horizontalPosition: 'right',
    });
  }

  warn(msg: string) {
    this._snackBar.openFromComponent(NotifyComponent, {
      duration: 3000,
      data: msg,
      panelClass: ['bg-warning'],
      horizontalPosition: 'right',
    });
  }
}
