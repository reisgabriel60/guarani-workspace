import {
  Component,
  EventEmitter,
  Input,
  Output,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'workspace-paginator',
  templateUrl: './paginator.component.html',
  styleUrls: ['./paginator.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class PaginatorComponent {
  @Input() paginate = {
    itemsPerPage: 5,
    currentPage: 1,
    totalItems: 0,
  };

  @Output() pageChange: EventEmitter<number> = new EventEmitter<number>();

  pages(): number[] {
    if (!this.paginate.totalItems) return [1];

    return Array.from(
      {
        length: Math.ceil(
          this.paginate.totalItems / this.paginate.itemsPerPage,
        ),
      },
      (x, i) => i + 1,
    );
  }

  getMessageCurrentPagination() {
    const currentPage = +this.paginate.currentPage;
    const itemsPerPage = +this.paginate.itemsPerPage;
    const totalItens = +this.paginate.totalItems;

    const currentItems = currentPage * itemsPerPage - itemsPerPage;
    const totalCurrentItens = currentItems + itemsPerPage;
    if (totalCurrentItens > totalItens) {
      return (
        'Mostrando de ' +
        (currentItems + 1) +
        ' a ' +
        totalItens +
        ' de ' +
        totalItens
      );
    } else {
      return (
        'Mostrando de ' +
        (currentItems + 1) +
        ' a ' +
        totalCurrentItens +
        ' de ' +
        totalItens
      );
    }
  }

  isFirstPage() {
    return this.paginate.currentPage === 1;
  }

  previous() {
    this.pageChange.emit(this.paginate.currentPage - 1);
  }

  setCurrent(page: number) {
    this.pageChange.emit(page);
  }

  isLastPage() {
    return this.paginate.currentPage === this.pages().slice(-1)[0];
  }

  next() {
    this.pageChange.emit(this.paginate.currentPage + 1);
  }
}
