import { Component, Input } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';
import { BlogPostInterface } from '@workspace/models';

@Component({
  selector: 'workspace-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.scss'],
})
export class PostComponent {
  @Input() model!: BlogPostInterface;

  constructor(private sanitizer: DomSanitizer) {}

  public get safeHtml(): SafeHtml {
    if (this.model && this.model.content)
      return this.sanitizer.bypassSecurityTrustHtml(this.model.content);
    else {
      return '';
    }
  }
}
