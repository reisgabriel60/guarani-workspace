import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { ExtendedModule, FlexModule } from '@angular/flex-layout';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { NgxUsefulSwiperModule } from 'ngx-useful-swiper';

import { ListSignaturesComponent } from './list-signatures/list-signatures.component';
import { TabsModule } from './tabs/tabs.module';
import { PostComponent } from './post/post.component';
import { NotifyComponent } from './notify/notify-bar.component';
import { PaginatorComponent } from './paginator/paginator.component';
import { NgxPaginationModule } from 'ngx-pagination';

@NgModule({
  imports: [
    CommonModule,
    TabsModule,
    MatButtonToggleModule,
    FormsModule,
    NgxUsefulSwiperModule,
    FlexModule,
    ExtendedModule,
    RouterModule,
    NgxPaginationModule,
  ],
  declarations: [
    ListSignaturesComponent,
    PostComponent,
    NotifyComponent,
    PaginatorComponent,
  ],
  exports: [
    ListSignaturesComponent,
    TabsModule,
    PostComponent,
    PaginatorComponent,
  ],
})
export class SiteSharedModule {}
