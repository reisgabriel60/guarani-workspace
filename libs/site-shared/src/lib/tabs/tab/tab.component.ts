import { Component, Input } from '@angular/core';

@Component({
  selector: 'workspace-tab',
  templateUrl: './tab.component.html',
  styleUrls: ['./tab.component.scss'],
})
export class TabComponent {
  @Input() tabTitle = '';
  @Input() active = false;
}
