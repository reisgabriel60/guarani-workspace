import {
  AfterContentInit,
  Component,
  ContentChildren,
  EventEmitter,
  Output,
  QueryList,
} from '@angular/core';
import { TabComponent } from './tab/tab.component';

@Component({
  selector: 'workspace-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss'],
})
export class TabsComponent implements AfterContentInit {
  @ContentChildren(TabComponent) tabs!: QueryList<TabComponent>;
  @Output() tabChange = new EventEmitter<string>();
  ngAfterContentInit() {
    const activeTabs = this.tabs.filter((tab) => tab.active);

    if (activeTabs.length === 0) {
      this.tabs.first.active = true;
    }
  }

  selectTab(tab: TabComponent) {
    this.tabs.toArray().forEach((tab) => (tab.active = false));
    tab.active = true;
    this.tabChange.emit(tab.tabTitle);
  }

  retornaStyleLable(i: number) {
    if (i === 0) {
      return 'border-radius: 24px 0 0 0';
    } else if (i === this.tabs.length - 1) {
      return 'border-radius: 0 24px 0 0';
    } else {
      return '';
    }
  }
}
