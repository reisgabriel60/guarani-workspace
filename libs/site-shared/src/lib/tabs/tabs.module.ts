import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TabsComponent } from './tabs.component';
import { TabComponent } from './tab/tab.component';
import { FlexModule } from '@angular/flex-layout';

@NgModule({
  declarations: [TabsComponent, TabComponent],
  exports: [TabsComponent, TabComponent],
  imports: [CommonModule, FlexModule],
})
export class TabsModule {}
